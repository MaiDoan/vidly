namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMembershipType : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MembershipTypes (Id,SignUpFree,DurationInMonths,DiscountRate) VALUES (1,1,0,0)");
            Sql("INSERT INTO MembershipTypes (Id,SignUpFree,DurationInMonths,DiscountRate) VALUES (2,13,40,10)");
            Sql("INSERT INTO MembershipTypes (Id,SignUpFree,DurationInMonths,DiscountRate) VALUES (3,30,10,20)");
            Sql("INSERT INTO MembershipTypes (Id,SignUpFree,DurationInMonths,DiscountRate) VALUES (4,90,30,30)");
        }
        
        public override void Down()
        {
        }
    }
}

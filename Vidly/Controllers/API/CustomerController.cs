﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dto;
using Vidly.Models;

namespace Vidly.Controllers.API
{
    public class CustomerController : ApiController
    {
        private VidlyDbContext context;
        public CustomerController()
        {
            context = new VidlyDbContext();
        }

        //Get/API/Customer
        public IEnumerable<CustomerDto> GetCustomer()
        {
            return context.Customers.ToList().Select(Mapper.Map<Customer,CustomerDto>);
        }

        //Get/API/Customer/2
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = context.Customers.FirstOrDefault(c => c.Id == id);
            if (customer == null)
                return NotFound();
            return Ok(Mapper.Map<Customer, CustomerDto>(customer));

        }

        //Post/Api/Customer
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            context.Customers.Add(customer);
            context.SaveChanges();
            customerDto.Id = customer.Id;
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);
        }

        //Put/Api/Customer/1
        [HttpPost]
        public void UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var CustomerInDb = context.Customers.FirstOrDefault(c => c.Id == id);
            if(CustomerInDb == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            Mapper.Map<CustomerDto, Customer>(customerDto, CustomerInDb);

            //CustomerInDb.Name = customer.Name;
            //CustomerInDb.Birthday = customer.Birthday;
            //CustomerInDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
            //CustomerInDb.MembershipTypeId = customer.MembershipTypeId;
            context.SaveChanges();
        }

        //Delete/Api/Customer/1
        [HttpDelete]
        public void DeleteCustomer(int id)
        {
            var CustomerInDb = context.Customers.FirstOrDefault(c => c.Id == id);
            if (CustomerInDb == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            context.Customers.Remove(CustomerInDb);
            context.SaveChanges();
        }
    }
}
